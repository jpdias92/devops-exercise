# All the continuouses

In this exercise we create a CI/CD pipeline using GitLab CI. The pipeline is defined by the file `.gitlab-ci.yml` in the repo's root.

## Features

- Run a markdown linter to verify all markdown files in the repo
- Run a static code validation using [KubeLinter](https://docs.kubelinter.io/#/) on the k8s manifests from [k8s FTW](../k8s-ftw/k8s-ftw.md)
- Build docker image from the [Docker-ayes exercise](../docker-ayes/docker-ayes.md)
- Run a simple test on the docker image from the [Docker-ayes exercise](../docker-ayes/docker-ayes.md)
  - Just checking if retrieving the blockchain info is successful after running the container
- Run python unit tests on python code from [Script grown-ups](../script-grown-ups/script-grown-ups.md)
- Publish the docker image from the [Docker-ayes exercise](../docker-ayes/docker-ayes.md) on Docker Hub
  - The DockerHub token and username are stored as GitLab CI variables and the token is masked to avoid showing up on logs
- Most of the jobs define rules so they only run when certain files are changed

## Possible improvements

- Improve tests to the image
- When the code is merged to the main branch, we should use a special tag for the image, to be clear that it is a proper release
- Deploy the StatefulSet in Kubernetes
  - Since I have used a local k8s cluster, it wasn't very easy to have a step that interacts with a k8s cluster from GitLab, I could probably have set up a local agent but that seemed too much out of the scope of the exercise
