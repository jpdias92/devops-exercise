# Docker-ayes

This code will create a container that runs [Litecoin 0.18.1](https://github.com/litecoin-project/litecoin/releases/tag/v0.18.1).

- [Development Notes](#development-notes)
- [Build](#build)
- [Run](#run)
- [Test](#test)
  - [Security Scan](#security-scan)
    - [Snyk](#snyk)
    - [Anchore](#anchore)
  - [Confirm that the Litecoin blockchain is syncing](#confirm-that-the-litecoin-blockchain-is-syncing)

## Development Notes

- Create a non-root user, `litecoin`, to run the litecoin daemon
- Use Alpine as the base image, to keep the image small
  - Less stuff means reduced chance of having packages with vulnerabilities
  - Started by using `debian:buster-slim` but `docker scan` was reporting 98 vulnerabilities, many of those with high severity
- Install `glibc` due to known issues (and confirmed during development) when running Litecoin on Alpine
  - See Github Issue [Unable to run on Alpine](https://github.com/litecoin-project/litecoin/issues/407)
- Validate the downloaded release by comparing its SHA256 hash with the downloaded checksum
  - The downloaded checksum is validated by verifying its GPG signature
    - The Litecoin public key is kept in the repo so it can be used for the validation. I decided to download and store it in version control because I noticed some instability in the `pgp.mit.edu` host from where I was downloading the GPG public key during development.

## Build

To build the image, run:

```
docker build docker-ayes -f docker-ayes/Dockerfile -t jpdias92/litecoin:0.18.1
```

## Run

To start Litecoin 0.18.1 daemon in a container, run: `docker run jpdias92/litecoin:0.18.1`

To persist the downloaded Litecoin blockchain locally, we can specify a volume when running the image. This prevents having to download the entire blockchain from the beginning, every time we restart the container

```
docker run -v <host_directory>:/home/litecoin/.litecoin jpdias92/litecoin:0.18.1
```

## Test

### Security Scan

#### Snyk

To test the image for vulnerabilities using the default Docker integration with Snyk, run:

```
docker scan jpdias92/litecoin:0.18.1
```

It returns `Tested 46 dependencies for known vulnerabilities, no vulnerable paths found.`

#### Anchore

The image also passes Anchore's vulnerability scans and policy evaluations. To reproduce this test:

1. Set up Anchore in local environment following their [Quickstart guide using Docker Compose](https://engine.anchore.io/docs/quickstart/)
1. Wait for all vulnerability data to become synced
1. Running a vulnerability scan with `docker-compose exec api anchore-cli image vuln jpdias92/litecoin:0.18.1 all` returns nothing
1. Running a policy evaluation with `docker-compose exec api anchore-cli evaluate check jpdias92/litecoin:0.18.1` returns `Status: pass`

### Confirm that the Litecoin blockchain is syncing

A basic test is to make sure that, once the Litecoin daemon starts, it makes progress syncing the Litecoin blockchain. We can use the below command for that:

```
docker exec -it <container_id> sh -c "litecoin-cli getblockchaininfo"
```

We can look at the `headers` and `blocks` count for example to ensure the sync is progressing. At the beginning, only the `headers` count increases. After some minutes, both counters should start to increase.
