# Script kiddies

In this exercise we propose a text manipulation problem and its corresponding solution.

- [Problem](#problem)
- [Solution](#solution)
  - [Explanation](#explanation)
- [Development Notes](#development-notes)

## Problem

Extract a list of tx numbers and corresponding dates from Litecoin's daemon log that are in the format:

```
2021-08-19T12:21:13Z UpdateTip: new best=f599ad368353f7d77c1b436bce3068f13b8bce461b95d78469e1d8f48d903bb9 height=1427954 version=0x20000000 log2_work=71.32417 tx=24627338 date='2018-05-25T16:55:42Z' progress=0.402527 cache=134.2MiB(984796txo)
```

**Requirements:**

- Remove all extra characters and information, keep only the tx number and the date
- Each row should be in the format `<tx_number>;<date>`
- Discard all log lines that don't contain this information about transactions and dates.
- Use the file `litecoin.log` as an input
- Save the resulting list to a `.csv` file

The example log line should become `24627338;2018-05-25T16:55:42Z`.

## Solution

```
awk '/date=/ && /tx=/ {print $8 $9}' litecoin.log |
  tr -d "'" |
  sed 's/date=/;/' |
  sed 's/tx=//' > result.csv
```

### Explanation

- `awk '/date=/ && /tx=/ {print $8 $9}'`
  - Filter just the rows that contain tx numbers and dates.
  - Retrieve the fields corresponding to tx number and date (8th and 9th fields) if we split each line using space as separator
- `tr -d "'"`
  - Remove the single quotes enclosing the date value
- `sed 's/date=/;/'`
  - Remove the `date=` string and replace it by the desired separator character, a semicolon `;`
- `sed 's/tx=//'`
  - Remove the `tx=` string

## Development Notes

- There were other options of using the requested tools to solve this problem. To name a few:
  - We could have used `grep` to filter the lines that contain `date=` and `tx=` but using `awk` allowed me to easily add the `AND` condition
  - We could have removed the `tx=` string with `tr -d` instead of using `sed` to replaced it with an empty string
  - Likewise, we could have used `sed` instead of `tr -d` to remove the single quotes
- The filtering condition (a log line containing both `date=` and `tx=`) might not be bullet-proof but could be easily improved by making the condition even more specific
