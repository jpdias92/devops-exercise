# k8s FTW

In this exercise we deploy Litecoin as a Kubernetes StatefulSet.

## Development Notes

- Developed and tested on the Kubernetes cluster that comes integrated with Docker Desktop
- Regarding storage, this k8s cluster comes with a `hostpath` storage class configured and ready to use. For simplicity sake, I'm using this one, although it only works in single node k8s cluster and is only suitable for development purposes
  - If I had access to a managed k8s service in one of the major clouds, it would be trivial to use one of the storage classes provided by default with those services instead of `hostpath`
  - After deploying the StatefulSet, a PVC and a PV will be automatically provisioned by Docker Desktop. As long as the PVC is not deleted, the data will persist across pod restarts
- About the resources, I chose the values based on some examples from the Internet and on trial and error.
  - Initially, the memory limit was set to 1Gi. I noticed the pod starting to get OOM killed a lot after some time with this value so I increased it to 2Gi and seems to be running fine now.

## Run

Deploy Litecoin's StatefulSet:

```
kubectl apply -f k8s-ftw/litecoin-statefulset.yaml
```

## Test

Check the blockchain sync status to ensure it is progressing:

```
kubectl exec --stdin --tty litecoin-0 -- /bin/sh -c "litecoin-cli getblockchaininfo"
```
