# Terraform lovers unite

In this exercise we deploy 4 IAM resources using Terraform.

## Development Notes

- A local module, [iam-resources](./modules/iam-resources/README.md) was created for this purpose, and is called by the root Terraform module in this directory.
- The only input variable is the environment, which defaults to `dev`

## AWS Authentication

> Make sure you initialize the Terraform working directory by running `terraform init`

Remember to authenticate in your AWS account before running Terraform. There are several ways of doing that, as described in [AWS Provider - Authentication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication).

## Provisioning

```
terraform plan
terraform apply
```

## Removing

```
terraform destroy
```
