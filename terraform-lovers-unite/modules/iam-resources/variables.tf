# Input variable definitions

variable "role_name" {
  description = "Name of the IAM CI role."
  type        = string
}

variable "policy_name" {
  description = "Name of the IAM CI policy."
  type        = string
}

variable "group_name" {
  description = "Name of the IAM CI user group."
  type        = string
}

variable "user_name" {
  description = "Name of the IAM CI user."
  type        = string
}

variable "tags" {
  description = "Tags to set on the role."
  type        = map(string)
  default     = {}
}
