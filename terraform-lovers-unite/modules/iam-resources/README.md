# IAM Resources

This module provisions:

- A role, with no permissions, which can be assumed by users within the same account
- A policy, allowing users / entities to assume the above role
- A group, with the above policy attached
- A user, belonging to the above group
