data "aws_caller_identity" "current" {}

resource "aws_iam_role" "ci-role" {
  name = var.role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = data.aws_caller_identity.current.arn  # Makes the current account a trusted identitiy that can assume the role
        }
      },
    ]
  })

  tags = var.tags
}

resource "aws_iam_policy" "ci-policy" {
  name        = var.policy_name
  description = "CI policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Resource = aws_iam_role.ci-role.arn  # Creates a policy to allow assuming the ci-role previously created
      },
    ]
  })

  tags = var.tags
}

resource "aws_iam_group" "ci-group" {
  name = var.group_name
}

resource "aws_iam_group_policy_attachment" "ci-policy-group-attach" {
  group      = var.group_name
  policy_arn = aws_iam_policy.ci-policy.arn
}

resource "aws_iam_user" "ci-user" {
  name = var.user_name

  tags = var.tags
}

resource "aws_iam_user_group_membership" "ci-user-group" {
  user = "${var.user_name}"

  groups = [
    var.group_name
  ]

  depends_on = [
    aws_iam_user.ci-user,
    aws_iam_group.ci-group
  ]
}
