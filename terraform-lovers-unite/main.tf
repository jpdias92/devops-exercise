provider "aws" {
  region = "eu-west-2"
}

module "iam_resources" {
  source = "./modules/iam-resources"

  role_name = "${var.environment}-ci-role"
  policy_name = "${var.environment}-ci-policy"
  group_name = "${var.environment}-ci-group"
  user_name = "${var.environment}-ci-user"

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}
