# Script grown-ups

In this exercise we use Python to solve the problem stated in [Script kiddies - Problem](../script-kiddies/script-kiddies.md#problem). The solution can be found in `script_grownups.py`.

## Development Notes

- Developed using Python 3.9.6
- Added some very simple unit tests
- The goal of splitting this simple logic into 2 functions is to allow the code to be more easily tested.
  - The `process_log_line` function has no side effects (reading/writing files) so it's much more easy to test in isolation
- A good improvement for the tests would be to also test the function that handles files, `extract_tx_and_date_to_csv`
- Processing the file line by line should ensure that the code would work fine even for very large files

## Run

```
python3 script_grownups.py
```

## Test

```
python3 -m unittest
```
