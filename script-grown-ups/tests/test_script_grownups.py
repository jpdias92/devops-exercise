from unittest import TestCase

import script_grownups


class TestExtractTxDate(TestCase):

    def test_process_log_line_happy_path(self):
        input = "2021-08-19T12:21:13Z UpdateTip: new best=f599ad368353f7d77c1b436bce3068f13b8bce461b95d78469e1d8f48d903bb9 height=1427954 version=0x20000000 log2_work=71.32417 tx=24627338 date='2018-05-25T16:55:42Z' progress=0.402527 cache=134.2MiB(984796txo)"
        expected_output = '24627338;2018-05-25T16:55:42Z\n'
        self.assertEqual(expected_output, script_grownups.process_log_line(input), 'Happy Path')

    def test_process_log_line_filter(self):
        input = "021-08-19T19:11:28Z Opening LevelDB in /home/litecoin/.litecoin/chainstate"
        self.assertEqual(None, script_grownups.process_log_line(input), 'Log line should be filtered out')
