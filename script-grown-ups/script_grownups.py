from pathlib import Path

# Constants
LITECOIN_LOGFILE_PATH = "../script-kiddies/litecoin.log" # relative path to script
RESULT_FILE_PATH = "result.csv" # relative path to script


def extract_tx_and_date_to_csv(input_file_path, result_file_path):

  with open(result_file_path, 'w') as result_file:
    for line in open(input_file_path):
      output_line = process_log_line(line)
      if output_line:
        result_file.write(output_line)


def process_log_line(line):
  # Only process lines that contain "tx=" and "date="
  if not ('tx=' in line and 'date=' in line):
    return None

  line_contents = line.split()
  filtered_fields = line_contents[7] + line_contents[8]  # 7th position is tx and 8th position is date
  return filtered_fields.replace('date=', ';').replace('tx=','').replace("'",'') + '\n'


def main():
  # Calculate absolute paths for input and output files
  input_file_path = (Path(__file__).parent / LITECOIN_LOGFILE_PATH).resolve()
  result_file_path = (Path(__file__).parent / RESULT_FILE_PATH).resolve()

  extract_tx_and_date_to_csv(input_file_path, result_file_path)


if __name__ == "__main__":
    main()
