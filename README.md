# devops-exercise

This repo contains the solutions to the DevOps test questions:

1. [Docker-ayes](./docker-ayes/docker-ayes.md)
1. [k8s FTW](./k8s-ftw/k8s-ftw.md)
1. [All the continuouses](./all-the-continuouses/all-the-continuouses.md)
1. [Script kiddies](./script-kiddies/script-kiddies.md)
1. [Script grown-ups](./script-grown-ups/script-grown-ups.md)
1. [Terraform lovers unite](./terraform-lovers-unite/terraform-lovers-unite.md)

## Developed Using

- MacBook Pro running macOS Big Sur 11.4
- Docker Desktop for Mac 3.5.2
- zsh 5.8
- GitLab
- Python 3.9.6
- Terraform 1.0.5
- AWS
